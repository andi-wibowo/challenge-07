// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';
import AllCar from './component/pages/all-car';
import RentCar from './component/pages/rent-car';

function App() {
  return (
    <div className="App">
      <Router>
            <Routes>
              <Route path="/" element={<AllCar />} />
              <Route path="/sewa" element={<RentCar />} />
            </Routes>
          </Router>
    </div>
  );
}

export default App;