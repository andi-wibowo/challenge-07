import React from 'react';

const Banner = () => {

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
            <a className="navbar-brand" href="/">
                <img src="../../assets/vector-logo.png" height="100%" alt="" />
            </a>
            <button className="navbar-toggler mb-4" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul className="nav nav-pills">
                    <li className="nav-item">
                        <a className="nav-link link tebel-sedang" href="#ourservices">Our Services &nbsp;&nbsp;</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link link tebel-sedang" href="#why">Why Us &nbsp;&nbsp;</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link link tebel-sedang" href="#testi">Testimonial &nbsp;&nbsp;</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link link tebel-sedang" href="#faq">FAQ &nbsp;&nbsp;</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link active tebel-sedang bg-hijau" href="/">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main>
    <section id="ourservices">
    <div className="container"> 
        <div className="row">
            <div className="col-lg-6 col-md-6 mt-5">
                <h1 className="tebel-banget"><b>Sewa & Rental Mobil Terbaik di<p>Kawasan (Lokasimu )</p></b></h1>
                <div className="desc mt-4">
                    <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani 
                        kebutuhanmu untuk sewa mobil selama 24 jam.
                    </p>
                </div>
                <div className="tombol mt-5">
                    <a href="/sewa" className="button shadow tebel-sedang">Mulai Sewa Mobil</a>
                </div>
            </div>
            <div className="col-lg-6 col-md-6 mt-5">
                <img src="../../assets/mobil-1.png" className="img" alt='' />
            </div>  
        </div>
    </div>
    </section>
    </main>
        </div>
    );
}

export default Banner;