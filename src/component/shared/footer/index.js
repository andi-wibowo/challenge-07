import React from 'react';

const Footer = () => {
    return (
        <div>
            <div className="container">
        <div className="row">
            <div className="footer-col mt-5">
                <h4>company</h4>
                <ul>
                    <li>Jalan Suroyo No. 161 Mayangan Kota Probolinggo 67200</li>
                    <li>binarcarrental@gmail.com</li>
                    <li>081-233-334-808</li>
                </ul>
            </div>
            <div className="footer-col mt-5">
                <h4>get help</h4>
                <ul>
                    <li><a href="#ourservices">Our Services</a></li>
                    <li><a href="#why">Why Us</a></li>
                    <li><a href="#testi">Testimonial</a></li>
                    <li><a href="#faq">FAQ</a></li>
                </ul>
            </div>
            <div className="footer-col mt-5">
                <h4>Connect with us</h4>
                <div class="social-links">
                    <a href="/"><i className="fab fa-facebook-f"></i></a>
                    <a href="/"><i className="fab fa-instagram"></i></a>
                    <a href="/"><i className="fab fa-twitter"></i></a>
                    <a href="/"><i className="fab fa-telegram"></i></a>
                    <a href="/"><i className="fab fa-twitch"></i></a>
                </div>
            </div>
            <div className="footer-col mt-5">
                <h4>&copy; Copyright Binar 2022</h4>
                <ul>
                    <li><img src="../../assets/vector-logo.png" height="100%" alt='' /></li>
                </ul>
            </div>
        </div>
    </div>
        </div>
    );
}
export default Footer;