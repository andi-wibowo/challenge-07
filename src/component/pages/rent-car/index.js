import React, { useEffect, useState } from 'react';
import Bannercari from "../../shared/bannercari";
import axios from 'axios';
import moment from 'moment';

const RentCar = () => {
    const [cars, setCars] = useState([]);
    const [filterCar, setFilterCar] = useState({
        driver: '',
        tanggal: '',
        waktu: '',
        penumpang: 0
    });

    useEffect(() => {
        axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
            .then(response => {
                setCars(response.data);
            }).catch(err => {
                console.error(err);
            });
    }, []);


    const getDriverType = (event) => {
        setFilterCar({
            driver: event.target.value
        });
    }

    const getDateRent = (event) => {
        setFilterCar((prevState) => ({
            ...prevState,
            tanggal: event.target.value
        }));
    }

    const getTimeRent = (event) => {
        setFilterCar((prevState) => ({
            ...prevState,
            waktu: event.target.value
        }));
    }

    const getPassengerCount = (event) => {
        setFilterCar((prevState) => ({
            ...prevState,
            penumpang: parseInt(event.target.value)
        }));
    }

    const doFilterCars = () => {
        const filteredCarsData = cars.filter((item) => item.capacity === filterCar.penumpang && moment(item.availableAt).format("L") === moment(filterCar.tanggal).format("L"));

        console.info(filteredCarsData);

        setCars(filteredCarsData);

        cars.forEach(val => {
            console.info("PARSED DATE ITEM : ", moment(val.availableAt).format("L"));
            console.info("PARSED DATE FILTER PARAM : ", moment(filterCar.tanggal).format("L"));
        })
    }

    return (
        <div>
            <Bannercari />
            {/* TODO: Filter data cars */}
            <div class="container cari-hasil">
            <div class="row">
                <div class="col-md-12 col-lg-12">
            <div class="card shadow cari-card">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                    <div class="col-lg-3 col-md-3 mt-3">
                        <label for="" class="type">Tipe Driver</label>
                        <select id="driver" class="form-select type-content" onclick={event => getDriverType(event)}>
                            <option selected disabled>Pilih Tipe Driver</option>
                            <option value="Dengan Supir">Dengan Supir</option>
                            <option value="Tanpa Supir (Lepas Kunci)">Tanpa Supir (Lepas Kunci)</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-2 mt-3">
                        <label for="" class="type">Tanggal</label>
                            <input id="pesan" type="date" onclick={event => getDateRent(event)} class="tanggal-cari" />
                    </div>
                    <div class="col-lg-3 col-md-2 mt-3">
                        <label for="" class="type">Waktu Jemput/Ambil</label>
                        <select id="waktu" class="form-select type-content" onclick={event => getTimeRent(event)}>
                            <option selected disabled>Pilih Waktu</option>
                            <option value="08:00">08:00 WIB</option>
                            <option value="09:00">09:00 WIB</option>
                            <option value="10:00">10:00 WIB</option>
                            <option value="11:00">11:00 WIB</option>
                            <option value="12:00">12:00 WIB</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 mt-3">
                        <label for="" class="type">Jumlah Penumpang (optional)</label>
                        <input id="penumpang" type="number" onChange={event => getPassengerCount(event)} className="tanggal-cari" />
                    </div>
                    <div class="col-md-12 col-lg-12 mt-4"><button type="button" class="btn btn-success tebel-sedang bg-hijau" onClick={() => doFilterCars()}>Cari Mobil</button>
                    </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
            </div>
        </div>
        </div>
    )
};

export default RentCar;