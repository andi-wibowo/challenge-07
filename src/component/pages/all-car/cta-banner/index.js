const CtaBanner = () => {
    return (
        <>
            <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 mt-5">
            <div class="card-aja" align="center" style={{width: "auto", height: "20rem", backgroundColor: "#0D28A6"}}>
            <div class="card-body" style={{paddingTop: "7%"}}>
              <h1 class="card-title-judul" style={{color: "white"}}>Sewa Mobil di (Lokasimu) Sekarang</h1>
              <p class="card-text-teks" style={{color: "white"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br />
              tempor incididunt ut labore et dolore magna aluqua</p>
              <a href="/sewa" class="button tebel-sedang">Mulai Sewa Mobil</a>
            </div>
        </div>
        </div>
    </div>
    </div>
    </section>
        </>
    );
}

export default CtaBanner;