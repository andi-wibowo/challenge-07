const WhyUs = () => {
    return (
        <div>
            <section id="why">
        <div className="container">
            <div className="row mt-5 mb-5">
                <h1 className="display-1 text-truncate tebel-banget"><b>Why Us?</b></h1>
            <div className="desc">
                <p>Mengapa harus pilih Binar Car Rental?</p>
            </div>
            <div className="row">
                <div className="col-md-6 col-lg-3 mt-3">
                <div className="card" style={{width: "19rem", height: "12rem"}}>
                <div className="card-body" style={{paddingTop: "15%"}}>
              <h5 className="card-title"><i class="fa fa-thumbs-up" aria-hidden="true"></i>
                <b> Mobil Lengkap</b></h5>
              <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
            </div>
        </div>
        </div>
        <div className="col-md-6 col-lg-3 mt-3">
        <div className="card" style={{width: "19rem", height: "12rem"}}>
            <div className="card-body" style={{paddingTop: "15%"}}>
              <h5 className="card-title"><i className="fa fa-tag" aria-hidden="true"></i>
                <b> Harga Murah</b></h5>
              <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
            </div>
        </div>
        </div>
        <div className="col-md-6 col-lg-3 mt-3">
        <div className="card" style={{width: "19rem", height: "12rem"}}>
            <div className="card-body" style={{paddingTop: "15%"}}>
              <h5 className="card-title"><i className="fa fa-clock" aria-hidden="true"></i>
                <b> Layanan 24 Jam</b></h5>
              <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia diakhir minggu</p>
            </div>
        </div>
        </div>
        <div className="col-md-6 col-lg-3 mt-3">
        <div className="card" style={{width: "19rem", height: "12rem"}}>
            <div className="card-body" style={{paddingTop: "15%"}}>
              <h5 className="card-title"><i className="fa fa-certificate" arial-hidden="true"></i><b> Sopir Profesional</b></h5>
              <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
            </div>
            </div>
        </div>
        </div>
        </div>
        </div>
    </section>
    </div>
    );
}

export default WhyUs;