const Testimoni = () => {
    return (
        <>
            <section id="testi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 mt-5">
                    <h1 class="testimoni-tebel"><b>Testimonial</b></h1>
                    <p class="testi">Berbagai review positif dari para pelanggan kami</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 mt-2">
                    <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-label="Slide 1"></li>
                          <li data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang1.jfif" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang2.jfif" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang3.png" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang4.png" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-half" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang5.png" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="single-box">
                                            <div class="img-area"><img src="assets/orang6.png" alt="" /></div>
                                            <div class="img-text">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star-half" aria-hidden="true"></i>
                                                <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                                                    lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod"</h2>
                                            <p>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </div>
    </section>
        </>
    );
}

export default Testimoni;