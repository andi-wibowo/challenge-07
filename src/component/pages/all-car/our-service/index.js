const OurService = () => {
    return (
        <div>
            <div className="container">
        <div className="row">
            <div className="col-lg-6 mt-5">
                <img src="assets/orang-1.png" className="orang" alt="" />
            </div>
            <div className="col-lg-6 mt-5">
            <h1 className="tebel-banget"><b>Best Car Rental for any<p> kind of trip in (Lokasimu)!</p></b></h1>
                <div className="desc mt-2">
                    <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah 
                        dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                    </p>
                    <p><i className="fa fa-check-circle" style={{width: "7%"}} aria-hidden="true"></i>Sewa Mobil Dengan Supir di Bali 12 Jam
                    </p>
                    <p><i className="fa fa-check-circle" style={{width: "7%"}} aria-hidden="true"></i>Sewa Mobil Lepas Kunci di Bali 24 Jam
                    </p>
                    <p><i className="fa fa-check-circle" style={{width: "7%"}} aria-hidden="true"></i>Sewa Mobil Jangka Panjang Bulanan
                    </p>
                    <p><i className="fa fa-check-circle" style={{width: "7%"}} aria-hidden="true"></i>Gratis Antar - Jemput Mobil di Bandara
                    </p>
                    <p><i className="fa fa-check-circle" style={{width: "7%"}} aria-hidden="true"></i>Layanan Airport Transfer / Drop In Out
                    </p>
                </div>
            </div>
        </div>
    </div>
        </div>
    );
}

export default OurService;